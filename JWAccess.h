#ifndef JWACCESS_H_H_H
#define JWACCESS_H_H_H

#include <afxdb.h>

class JWAccess  
{
public:
	BOOL Connect(LPCTSTR szDbPath);
	void DisConnect();
	long Select(LPCTSTR szSQL);
	CString GetField(const SHORT fieldIndex);
	CString GetField(LPCTSTR fieldName);
	BOOL Query(LPCTSTR szSQL);
	
	static JWAccess* GetInstance();
	JWAccess();
	virtual ~JWAccess();
	
private:
	CDatabase	m_Database;		 //数据库对象
	CRecordset* m_pRecordSet;    //数据库记录集指针

};

#endif
